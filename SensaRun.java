package Sensa;
import java.io.*;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import options.Options;

/*
 * Revisions:
 * @August 29th by Hcai
 * 		- preProcessArg changed to adopt to Linux directory delimiter 
 *	@August 31st by Hcai
 *		- fix file handler leakages that halt the executions in the run-time 
 * @Sept. 7th by Hcai
 * 		- add the "different-change-per-occurrence-in-an-execution" option (by 
 * 			adding the "different_change_per_occurrence" property in sensa.cfg).
 * 			See the changeLog in Modify class for more details on this revision.
 *  @Sept. 9th by Hcai
 *  	- add a system property, "-DOutputScope", to indicate what to dump out
 *  			- 0 (default): dump the whole execution history only
 *  			- 1 :  dump the (stmtid->number of impacts) map structure only
 *  			- 2 : dump both of 0 and 1
 *  @Sept. 10th by Hcai
 *  	- optimize the Reduction process (i.e. the conversion of original execution history to (stmtid->#changes) map
 *  		to save the memory request peak in the Runtime (stimulus: OutOfMemeory still occurred even we now 
 *  		only need holding two 1.6G data in the memory,) : avoid memory copies with regards to the ExHist Maps by
 *  		passing references by value instead of returning them from the reader;
 *  	- add another property, a boolean, "-DForActualImpact", to accommodate the reduction of execution history 
 *  		output of ExecHistoryInstrumenter, which is to be used for actual impact analysis.
 *  	- add the optional fifth parameter to receive the second binPath in order to accommodate the needs of 
 *  		running the ExecHistoryInstrumented subject, where we need to run a single execution for the original 
 *  		and the modified version respectively so as to reduce the history output into the (stmtid->#impacts)
 *  	- add the optional sixth parameter to receive the output directory for the subject class of modified version
 *   @Sept. 13th by Hcai
 *   	- fix bug in the extended processing for "ForActualImpact" functionalities: close files after done
 */
public class SensaRun{

	static boolean differentChange = false;
	static boolean ifAllTest = true;
	static boolean ifStop = false, ifModify = true;
	static boolean bForActualImpact = false; // added by hcai
	static int testN;
	static int runM;
	static int runtime;
	static int test = -1;
	static int countMod = 1;
	// -- added by hcai
	static int outScope = 0;
	static int startId;
	// --
	static String subjectDir = "", ifOnly;
	static String outputrootDir = ""; //added by Siyuan
	// -- added by hcai for informing the location of the modified subject as opposed to the original one
	static String ModifiedBinPath = "";
	static String ModOutRootDir = "";
	// --
	static int callCounter = 0;
	// -- Siyuan: to reduce runs that doesn't hit the modification target
	static boolean runHit = true;
	// --
	
	public static void main(String args[]){
		
		/** java SensaRun subject directory
			directory should be like F:/coding/subjects/schedule1
			binPath indicates where the class file is
			inside the directory, it should contains folder bin, inputs, sensa.cfg
		*/
				
		ifOnly = System.getProperty("RunOneInMod");
		// we want the default is "Run Modification per Modify.modify invocation"
		if (null == ifOnly) {
			ifOnly = "false";
		}
		
		// unless specified, just adopt the default behavior - dump the original execution history
		String strOutScope = System.getProperty("OutputScope");
		if (null != strOutScope) {
			outScope = Integer.parseInt(strOutScope);
			
			if  ( 1 != outScope && 2 != outScope ) {
				outScope = 0;
			}
		}
		
		// for applying SensaRun to running the execHistoryInstrumented subject for later actual impact analysis
		String strActual = System.getProperty("ForActualImpact");
		if (null != strActual) {
			bForActualImpact = strActual.equalsIgnoreCase("true");
		}
		
		String subjectName = args[0];

		String dir = args[1];
		String binPath = args[2];
		SensaRun.subjectDir = dir;
		String verId = args[3];
		try{
		SensaRun.startId = Integer.parseInt(verId);
		}
		catch (NumberFormatException e){
			System.out.println("Ignore the verId comes to startId!");
		}
		
		//Siyuan added:
		if(args.length > 4){
			//pass the 4th argument as the output directory
			//if args doesn't have 4th arg, the output directory will be the same with the previous version 
			outputrootDir = args[4];
		}
		//end Siyuan
		
		// -- hcai
		if (args.length > 5) {
			ModifiedBinPath = args[5];
		}
		
		if (ModifiedBinPath.length() < 1 && bForActualImpact) {
			System.err.println("For the actual-impact-analysis-purpose runs, location for " +
						"both the original and modified subject class is required, but only got the first.");
			return;
		}
		
		if (args.length > 6) {
			ModOutRootDir = args[6];
		}
		if (bForActualImpact && ModOutRootDir.equals("")) {
			ModOutRootDir = dir + "/runs";
		}
		// --

		String mod_alg = "";
		
		System.out.println("Subject: " + subjectName + " Dir=" + dir + " binpath=" + binPath + " verId=" + verId);
		
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dir+"/sensa.cfg")));
			String cfg = br.readLine();
			while(cfg != null){

				if(cfg.indexOf("mod_alg:") != -1){
					mod_alg = cfg = br.readLine();
				}
				else if(cfg.equals("runs:")){					
					cfg = br.readLine();
					runtime = Integer.parseInt(cfg);
				}
				else if(cfg.equalsIgnoreCase("different_change_per_occurrence:")){					
					cfg = br.readLine();
					differentChange = (0!=Integer.parseInt(cfg));
				}

				else if(cfg.equals("test:")){
					cfg = br.readLine();
					if(cfg.equals("")){
						ifAllTest = true;
					}
					else {
						ifAllTest = false;
						test = Integer.parseInt(cfg);
					}
					break;
					
				}
				
				else cfg = br.readLine();
			}
			// --- moved up by hcai
			br.close();
			// ---
			if (bForActualImpact) {
			   /* Note: For actual impact analysis, we need only a single Run for each test input */
			   SensaRun.runtime = 1;
			}

			startRunSubject(subjectName, dir, binPath, verId, mod_alg);
			
			//br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void startRunSubject(String name, String dir, String binPath, String verId, String mod_alg){

		int n = 0;
		
		BufferedReader br;
		PrintStream stdout = System.out;
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dir+"/inputs/testinputs.txt")));
			String ts = br.readLine();
			while(ts != null){
				n++;

				if(SensaRun.test == -1 || SensaRun.test == n){
					String []args = preProcessArg(ts,dir);
					
					SensaRun.testN = n;
					
					//Siyuan: commented out
					//String outputDir = dir + "/runs" + "/" + verId + "-" + mod_alg + "/test"+n;
					//Siyuan added:
					String outputDir;
					if(outputrootDir.equals("")){
						//the same behavior with the previous version
						outputDir = dir + "/runs" + "/" + verId + "-" + mod_alg + "/test"+n;
					}else{
						outputDir = outputrootDir + "/" + verId + "-" + mod_alg + "/test"+n;
					}
					//end Siyuan
					
					File dirF = new File(outputDir);
					if(!dirF.isDirectory())	dirF.mkdirs();
					
					System.setOut(stdout);
					System.out.println("current at the test No.  " + n);
					
					// moved here from prior to this loop so that this structure can be recycled immediately after the previous
					// test input was finished.
					HashMap<Integer,Object> exHistOrig = null; 
					// -- Siyuan
					runHit = true;
					String notRunWithNotHit = System.getProperty("notRunWithNotHit");
					if(notRunWithNotHit!=null && notRunWithNotHit.equals("true")){
						runHit = false; //initialize its value with false(when the target is hit by execution, its value will be updated with true)
					}
					// --
					for(int i=1;i<=SensaRun.runtime;i++){
						SensaRun.callCounter = 0;

						SensaRun.ifModify = true;
						SensaRun.countMod = 1;
						
						if(SensaRun.ifStop){
							System.setOut(stdout);
							System.out.println("distinct values run out, bail out now...");
							SensaRun.ifStop = false;
							break;
						}
						
						SensaRun.runM = i;
						String outputF = outputDir + "/run" + i + ".out";
						String errF = outputDir + "/run" + i + ".err";
						
						File outputFile = new File(outputF);
						PrintStream out = new PrintStream(new FileOutputStream(outputFile)); 
						System.setOut(out); 
						
						File errFile = new File(errF);
						PrintStream err = new PrintStream(new FileOutputStream(errFile)); 
						System.setErr(err);
						
						File runSub = new File(binPath);
						URL url = runSub.toURL();        
					    URL[] urls = new URL[]{url};

					    // Create a new class loader with the directory

					    ClassLoader cl = new URLClassLoader(urls);	
					    Class cls = cl.loadClass(name);
					    
					    Method me=cls.getMethod("main",args.getClass());
					    /* for debug only
					    System.setOut(stdout);
					    System.out.println("To feed the subject: ");
					    for (int k=0; k < args.length; k++) {
					    System.out.print(args[k] + " ");
					    }
					    System.out.println();
					    */
					    
					   me.invoke(null, (Object)args);
					   // --- added by hcai
					   out.flush();
					   out.close();
					   err.close();
					   // ---
					   
					   //////////////// Reduction of the output to work around possible disk space scarcity //////////////////
					   
					   if ( bForActualImpact ) {
						   String ModoutputDir = ModOutRootDir + "/" + verId + "-" + mod_alg + "/test"+n;
						   File ModdirF = new File(ModoutputDir);
							if(!ModdirF.isDirectory())	ModdirF.mkdirs();
						    // now that the "original version" has run, turn to the modified version
						   	String ModoutputF = ModoutputDir + "/run" + i + ".out";
							String ModerrF = ModoutputDir + "/run" + i + ".err";
							
							File ModoutputFile = new File(ModoutputF);
							PrintStream Modout = new PrintStream(new FileOutputStream(ModoutputFile)); 
							System.setOut(Modout); 
							
							File ModerrFile = new File(ModerrF);
							PrintStream Moderr = new PrintStream(new FileOutputStream(ModerrFile)); 
							System.setErr(Moderr);
							
							File ModrunSub = new File(ModifiedBinPath);
							URL Modurl = ModrunSub.toURL();        
							URL[] Modurls = new URL[]{Modurl};
							
							// Create a new class loader with the directory
							
							ClassLoader Modcl = new URLClassLoader(Modurls);	
							Class Modcls = Modcl.loadClass(name);
							
							Method Modme=Modcls.getMethod("main",args.getClass());
				    
							Modme.invoke(null, (Object)args);
							Modout.flush();
							Modout.close();
							Moderr.close();
						    
							if (outScope != 0) {
								System.setOut(stdout);
								System.out.print("[Actual-Impact-Analysis-Purpose Run] Processing two outputs from the test No.  " + n);
								int t = handleOutput(SensaRun.startId, outputF, ModoutputF);
								if ( 0 != t ) {
									System.out.println(" -- Failed");
									break;
								}
								
								// remove the execution history files to save disk space if specified
						    	if ( 2 == outScope || (new File(outputF).delete() && new File(ModoutputF).delete()) ) {
						    		System.out.println(" -- Succeeded");
					    		}
						    	else {
						    		System.out.println(" -- Failed");
						    	}
							}
					   }
					   else {
						    if ( outScope != 0 ) {
						    	System.setOut(stdout);
								System.out.print("[Sensa-Predict-Impact-Analysis-Purpose Run] Processing output from the Run No. " + i + " of the test No.  " + n);
						    	if (1 == i) {
						    		ExecHistoryReader readerOrig = new ExecHistoryReader(startId);
						    		exHistOrig = new HashMap<Integer,Object>();
						    		readerOrig.readExHist(exHistOrig, outputF);
						    	}
						    	else {
								    // process the output from the Run done right now
									try {
										int t = handleOutput(SensaRun.startId, exHistOrig, outputF);
										if ( 0 != t ) {
											System.out.println(" -- Failed [return=" + t + "]");
											break;
										}
									}
									catch (Exception e) {
										System.out.println("Error occurred in the output handling...");
										e.printStackTrace();
									}
						    	}
						    	
						    	// remove the raw execution history file to save disk space if specified
						    	if ( 2 == outScope || new File(outputF).delete() ) {
						    		System.out.println(" -- Succeeded");
					    		}
						    	else {
						    		System.out.println(" -- Failed");
						    	}
						    }
						    
						    // just for debug
						   // System.setOut(stdout);
							//System.out.println("current at the run No.  " + i + " of " + SensaRun.runtime);
							//System.out.println("In the Run No.  " + i + " #invocation of modify is " + SensaRun.callCounter);
					   }
					   
					   if(runHit==false){
						   runHit = true;
						   break;
					   }
					}
					
					 if ( !bForActualImpact ) {
						 exHistOrig = null; // release memory immediately to avoid crash due to memory request peak
					 }
					 
					 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					
					 if(!ifAllTest && SensaRun.test == n) {
						System.setOut(stdout);
						System.out.println("!! Not all inputs exhausted...!!");
						break;
					}
				}
				
				ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * convert the execution history output to the (stmtid->#impacts) map
	 */
	// this version serves the reduction of ExecHistoryInstrumenter's outputs
	private static int handleOutput(int startId, String fileOrigOut, String fileModOut)
	{
		ExecHistoryReader readerOrig = new ExecHistoryReader(startId);
		HashMap<Integer,Object> exHistOrig = new HashMap<Integer,Object>();
		readerOrig.readExHist(exHistOrig, fileOrigOut);
		return handleOutput(startId, exHistOrig, fileModOut );
	}
	
	// this version serves the reduction of Sensa's outputs, since we won't to read the control history repetitively
	private static int handleOutput(int startId, HashMap<Integer,Object> exHistOrig, String fileModOut) 
	{	
		if (null == exHistOrig) return -1;
		
		// make sure the original file would not be counted.
		ExecHistoryReader readerMod = new ExecHistoryReader(startId);
		HashMap<Integer,Object> exHistMod = new HashMap<Integer,Object>(); 
		readerMod.readExHist(exHistMod, fileModOut);
		//if (null == exHistMod) return -1;
		
		//boolean flag = true;
		//int count_impacted = 0;
		
		Map<Integer,Integer> retMap = new HashMap<Integer, Integer>();
		// determine diffs for each point and update point diff counter
		Set<Integer> pnts = new HashSet<Integer>(exHistOrig.keySet());
		pnts.addAll(exHistMod.keySet());
		for (Integer pnt : pnts) {
			// one of the following lists/hashes might be null, but not both
			Object valsOrig = exHistOrig.get(pnt);
			Object valsMod = exHistMod.get(pnt);
			assert valsOrig != null || valsMod != null;

			Integer count = retMap.get(pnt);
			if (count == null) {
				count = 0;
			}
			
			if (valsOrig == null || !valsOrig.equals(valsMod)){
				retMap.put(pnt, ++count);
				/*
				if(flag){
					flag = false;
					count_impacted ++;
				}
				*/
			}
		}
		
		// release memory immediately to avoid "crash at the peak"
		readerMod = null;
		exHistMod = null;
		pnts = null;
		
		// create a separate file to hold the serialized map
		try {
			//String fnOutMap = fileModOut + ".map";
			// change the extension to fit the post-processor
			String fnOutMap = fileModOut.substring(0, fileModOut.lastIndexOf("/run")) + "/" +
						fileModOut.substring( fileModOut.lastIndexOf("/run") + 4 );
			//System.err.println("will write to " + fnOutMap);
			FileWriter fw = new FileWriter(new File(fnOutMap));
			BufferedWriter bw = new BufferedWriter(fw);
			
			// serialize the map structure into the disk file, --- we dump binary to save storage (later if needed)
			Set<Integer> keys = new HashSet<Integer>(retMap.keySet());
			for(Integer key : keys) {
				bw.write( key.toString()+":"+retMap.get(key).toString()+"\n" );
			}
			
			bw.close();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
			return -2;
		}
		
		// end
		return 0;
	}
	
	/**
	 * record the modified value having been tried and make sure the value used is unique in each test
	 * @param n indicates which test
	 * @return: if return false, it means that the value has been used before.
	 */
	public static boolean valuestried(int n, Object valuetried){
		String dir = SensaRun.subjectDir + "/valuetried/";
		File dir_ = new File(dir);
		if(!dir_.isDirectory()){
			dir_.mkdir();
		}
			
		String fileName = dir + "/valuestried" + n + ".out";
		try {
			
			if(SensaRun.runM == 1){
				FileWriter forrun1 = new FileWriter(new File(fileName));
				BufferedWriter bw1 = new BufferedWriter(forrun1);
				bw1.write("");
				bw1.close();
				forrun1.close();
				return true;
			}
			
			/* 
			 * by default, in a single Run, we want the modification is same across all invocations of 
			 * this modify() interface
			 */
			if (!SensaRun.differentChange && SensaRun.callCounter >= 2) {
				return true;
			}

			RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
			String value = randomFile.readLine();
			
			// -- this algo can cause the OVERALL distinct value picking very clumsy
			while(value != null && !value.equals(null)){
				// -- hcai
				if (null == valuetried && value.equals("null")) {
					randomFile.close();
					return false;
				}
				// ---
				if(value.equals(valuetried.toString())){
					// --- added by hcai
					randomFile.close();
					// --- 
					return false;
				}
				value = randomFile.readLine();
			}

			long fileLength = randomFile.length();
		    randomFile.seek(fileLength);
		    //String content = valuetried + "\r\n";
		    // -- hcai
		    String content;
		    if (null == valuetried) {
		    	content = "null\r\n";
		    }
		    else {
		    	content = valuetried + "\r\n";
		    }
		    // ---
		    randomFile.writeBytes(content);
		    randomFile.close();
		    return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static String[] preProcessArg(String arg,String dir){
		// --- changed by hcai
		// String s1 = arg.replaceAll("\\s+"," ");
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
		// --- 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
}
	
// the following snippet is ported from TestAdequacy.ExecHistoryDiff and tailored for special needs (the needs
// for saving disk space request
class ExecHistoryReader {
	private final static String DATA_PREFIX = "|||";
	private final int DATA_PREFIX_LEN = DATA_PREFIX.length();
	private int startId = 0;
	
	public ExecHistoryReader(int sId) {
		this.startId = sId;
	}
		
	/** Ignore data occurring BEFORE start point. */
	public int readExHist(HashMap<Integer,Object> exHist, String fpathname) {
		boolean reachedStart = false;
		if (null == exHist) return -1;
		try{
			//Map<Integer,Object> exHist = new HashMap<Integer,Object>();
			
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fpathname));
			String s;

			while ((s = reader.readLine()) != null) {
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX))
					continue;
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// parse value
				final String val = removeFinalAddress(s.substring(bracketPos + 4));
				
				// store exec hist entry point->val
				if (Options.useHashing()) {
					Long prevHash = (Long) exHist.get(signedPoint);
					long hash = (prevHash == null)? 0 : prevHash;
					exHist.put(signedPoint, hash + (long)val.hashCode());
				}
				else {
					List<String> valsForPnt = (List<String>) exHist.get(signedPoint);
					if (valsForPnt == null) {
						valsForPnt = new ArrayList<String>();
						exHist.put(signedPoint, valsForPnt);
					}
					valsForPnt.add(val);
				}
			}
			
			return 0;
		}
		catch (FileNotFoundException e) { return -2; }
		catch (IOException e) { throw new RuntimeException(e); }
	}
	
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr)
				s = s.substring(0, addrPos + 1);
		}
		return s;
	}
}; // end of the class ExecHistoryReader
