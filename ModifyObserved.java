package Sensa;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;


public class ModifyObserved implements IModify {

	@Override
	public int intModify(int pre, int runM, int x, int minLimit, int maxLimit) {
		int re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =Integer.parseInt(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public float floatModify(float pre, int runM, float x, float minLimit,
			float maxLimit) {
		float re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){ 
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =Float.parseFloat(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;

			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public long longModify(long pre, int runM, long x, long minLimit,
			long maxLimit) {
		long re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =Long.parseLong(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public short shortModify(short pre, int runM, short x, short minLimit,
			short maxLimit) {
		short re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =Short.parseShort(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public char charModify(char pre, int runM) {
		char re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =tmp.charAt(0);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public double doubleModify(double pre, int runM, double x, double minLimit,
			double maxLimit) {
		double re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =Double.parseDouble(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public byte byteModify(byte pre, int runM, byte x, byte minLimit,
			byte maxLimit) {
		byte re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){
				//if(i == SensaRun.countMod){
					re =Byte.parseByte(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public String stringModify(String pre, int runM) {
		String re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				if(i == SensaRun.runM-1){	
				//if(i == SensaRun.countMod){
					re =tmp;
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}

	@Override
	public boolean boolModify(boolean pre) {
		// TODO Auto-generated method stub
		//return false;
		boolean re = pre;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));
			int i = 1;
			String tmp = br.readLine();
			while(tmp != null){
				//if(i == SensaRun.countMod){
				if(i == SensaRun.runM-1){	
					re =Boolean.parseBoolean(tmp);
					SensaRun.countMod ++ ;
					break;
				}
				i++;
				tmp = br.readLine();
			}
			if(br.readLine() == null)
				SensaRun.ifStop = true;
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return re;
	}
	
	@Override
	public Object objectModify(Object pre){
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(SensaRun.subjectDir+"/observed_value.out")));

			String tmp = br.readLine();
			if((tmp.equals("!!!NULL")) && (pre != null)) {
				return null;
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pre;
	}

}
