package Sensa;

import java.util.ArrayList;
import java.util.List;

public class SensaOptions {
	private static boolean execHist = false;
	private static List<Integer> startStmtIds = null;
	
	public  static boolean doExecHist() { return execHist; }
	public static List<Integer> getStartStmtIds() { return startStmtIds; }
	
	public static String[] process(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		
		boolean allowPhantom = true;
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			if (arg.startsWith("-start:")) {
				assert startStmtIds == null;
				
				startStmtIds = dua.util.Util.parseIntList(arg.substring("-start:".length()));
				/* should the "start" parameter be passed onto DUAF? */
				//argsFiltered.add(arg);
			}
			else if (arg.equals("-exechist")) {
				execHist = true;
			}
			else {
				argsFiltered.add(arg);
			}
		}
		
		if (allowPhantom)
			argsFiltered.add("-allowphantom");
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}
	
}
