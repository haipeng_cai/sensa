package Sensa;
/*
 * By swhite_zyj July 2012
 * 
 * Contains modify method
 *
 * Revisions:
 * 	@August 31st by Hcai
 *		- fix file handler leakages that halt the executions in the run-time
 * @Sept. 6th by Hcai
 * 		- changes in  modify() made to make sure that the actual number of values used for modifications to be tried
 * 			reach what is specified in the configuration file by the "runs:" property, esp. for the Random and Inc algorithm 
 * 			for types of int, float, double, short and long. 
 * @Sept. 7th by Hcai
 * 		- add the options for "same-change-across-all-occurrences-in-a-same-execution", allowing users to adopt "
 * 			different changes for each invocation of modify() in a single Run while taking "a same value to be used for 
 * 			modifications across all invocations of modify() in a single Run
 * 		- move the "configuration parsing" out of the modify() interface and make it another method "readConf", then
 * 			call it only once so as to reduce runtime workload (we do not expect that
 * 			this configuration, which should be kept constant during an execution of SensaRun, changes in the runtime 
 * @Oct. 2nd by Hcai
 * 		 - fix Modify for boolean and Object types making sure that no useless Runs proceed when all possible
 * 			values for modification run out. 
 */ 

/**
 * If you want to extend Sensa to support more types, you need to modify the following files:
 * 		Modify.java
 *			-- where you can receive the previous value from preObject, and you must offer you value to returnObject
 *			-- There are two placed you need to modify, one is for runM == 1, the other is for the common situation.
 *			-- You could find the example in String type.
 *
 *		IModify.java and those .java file that inherit from the interface.
 *
 *	If you want to add your own modification algorithm, you need to modify the following files:
 *		Modify.java
 *			-- You need to add codes for variable 'algorithm' and 'mod_alg' to match your own algorithm
 *			-- The algorithm will be got from sensa.cfg 
 *		Create a new ModifyX.java for your algorithm
 *			-- You could inherit from IModify or ModifyDefault ( return the same value as provided )
 *			-- You should allow your algorithm to support all present types in IModify.
 */

import java.io.*;
import java.util.Random;


public class Modify {

	public static void __link() { }
	
	public static float returnFloat, preFloat, minLimitFloat = Float.MIN_VALUE, maxLimitFloat = Float.MAX_VALUE;
	public static int returnInt = 0, preInt = 0, minLimitInt = Integer.MIN_VALUE, maxLimitInt = Integer.MAX_VALUE;
	public static String returnString = "", preString = "";
	public static double returnDouble = 0.0, preDouble = 0.0, minLimitDouble = Double.MIN_VALUE, maxLimitDouble = Double.MAX_VALUE;
	public static short returnShort = 0, preShort = 0, minLimitShort = Short.MIN_VALUE, maxLimitShort = Short.MAX_VALUE;
	public static byte returnByte, preByte, minLimitByte = -128, maxLimitByte = 127;
	public static long returnLong = 0, preLong = 0, minLimitLong = Long.MIN_VALUE, maxLimitLong = Long.MAX_VALUE;
	public static char returnChar, preChar;
	public static boolean returnBool, preBool;
	
	public static Object returnObject, preObject;

	static Random r = new Random();
	
	// -- added by hcai, Sept. 07
	public static IModify mof = null;
	public static int callCounter = 0;
	private static float increFloat = 1.0f;
	private static int increInt = 1;
	private static double increDouble  = 1.0;
	private static short increShort = 1;
	private static byte increByte = 1;
	private static long increLong = 1;
	private static boolean bForcible = true;
	private static boolean bKeepPreValue = false;
	private final static int MAXTRIAL = 20, TIMEROUT=10000;
		
	private static void readConf(String type) {
		BufferedReader br;
		//IModify mof;
		String algorithm = "";
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(SensaRun.subjectDir+ "/sensa.cfg")));
			String cfg = br.readLine();
			while(cfg != null){

				if(cfg.indexOf("mod_alg") != -1){					
					cfg = br.readLine();
					algorithm = cfg;
					cfg = br.readLine();
				}

				else if(cfg.indexOf("increment") != -1){
					cfg = br.readLine();
					if(type.equals("float"))
						increFloat = Float.parseFloat(cfg);
					else if(type.equals("double"))
						increDouble = Double.parseDouble(cfg);
					else if(type.equals("short"))
						increShort = Short.parseShort(cfg);
					else if(type.equals("long"))
						increLong = Long.parseLong(cfg);
					else if(type != null)
						increInt = Integer.parseInt(cfg);		
					cfg = br.readLine();

				}
				else if(cfg.indexOf("min") != -1){
					cfg = br.readLine();
					if(type.equals("float"))
						minLimitFloat = Float.parseFloat(cfg);
					else if(type.equals("double"))
						minLimitDouble = Double.parseDouble(cfg);
					else if(type.equals("short"))
						minLimitShort = Short.parseShort(cfg);
					else if(type.equals("long"))
						minLimitLong = Long.parseLong(cfg);
					else if(type.equals("byte"))
						minLimitByte = Byte.parseByte(cfg);
					else if(type != null)
						minLimitInt = Integer.parseInt(cfg);
					
					cfg = br.readLine();
				}
				else if(cfg.indexOf("max") != -1){
					cfg = br.readLine();
					if(type.equals("float"))
						maxLimitFloat = Float.parseFloat(cfg);
					else if(type.equals("double"))
						maxLimitDouble = Double.parseDouble(cfg);
					else if(type.equals("short"))
						maxLimitShort = Short.parseShort(cfg);
					else if(type.equals("long"))
						maxLimitLong = Long.parseLong(cfg);
					else if(type.equals("byte"))
						maxLimitByte = Byte.parseByte(cfg);
					else if(type != null)
						maxLimitInt = Integer.parseInt(cfg);	
					
					cfg = br.readLine();
				}
				else cfg = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		boolean bForcible = true;
		final int MAXTRIAL = 20, TIMEROUT=10000;
		*/
	
		if(algorithm.equals("rand")) {
			mof = new ModifyRandom();
			if (!SensaRun.differentChange) {
				bKeepPreValue = true;
			}
		}
		else if(algorithm.equals("inc")) { 
			mof = new ModifyIncrement();
		}
		else {
			mof = new ModifyObserved();
			bForcible = false;
			if (!SensaRun.differentChange) {
				bKeepPreValue = true;
			}
		}	
	}
	
	public static void modify(String type){
		// -- Siyuan
		SensaRun.runHit = true; 
		// --
		SensaRun.callCounter++;
		/*
		float increFloat = 1.0f;
		int increInt = 1;
		double increDouble  = 1.0;
		short increShort = 1;
		byte increByte = 1;
		long increLong = 1;
		*/
		// read the conf upon the first invocation in the current SensaRun session 
		if (null == mof) {
			readConf(type);
		}

		int testN = SensaRun.testN;
		int runM = SensaRun.runM;

		// Modify should modify x only once per run
		if(SensaRun.ifOnly == null || !SensaRun.ifOnly.equals("false")){
			
			if(!SensaRun.ifModify){
				runM = 1;
				return;
			}
				
			else SensaRun.ifModify = false;
		}
		
		if(runM == 1){
			if(type.equals("int")){
				write_Obeserved(preInt);
				returnInt = preInt;
				SensaRun.valuestried(testN,returnInt);
			}
			else if(type.equals("float")){
				write_Obeserved(preFloat);
				returnFloat = preFloat;
				SensaRun.valuestried(testN,returnFloat);
			}
			else if(type.equals("double")){
				write_Obeserved(preDouble);
				returnDouble = preDouble;
				SensaRun.valuestried(testN,returnDouble);
			}
			else if(type.equals("long")){
				write_Obeserved(preLong);
				returnLong = preLong;
				SensaRun.valuestried(testN,returnLong);
			}
			else if(type.equals("byte")){
				write_Obeserved(preByte);
				returnByte = preByte;
				SensaRun.valuestried(testN,returnByte);
			}
			else if(type.equals("short")){
				write_Obeserved(preShort);
				returnShort = preShort;
				SensaRun.valuestried(testN,returnShort);
			}
			else if(type.equals("char")){
				write_Obeserved(preChar);
				returnChar = preChar;
				SensaRun.valuestried(testN,returnChar);
			}
			else if(type.equals("boolean")){
				write_Obeserved(preBool);
				returnBool = preBool;
				SensaRun.valuestried(testN,returnBool);
			}
			else if(preObject instanceof String){
				preString = (String)preObject;
				write_Obeserved(preString);
				returnString = preString;
				returnObject = returnString;
				SensaRun.valuestried(testN,returnString);
			}
			
			else {
				if(preObject == null)
					write_Obeserved(preObject);
				returnObject = preObject;
				SensaRun.valuestried(testN,returnObject);
			}
			return;
		}
		
		/* --- moved out of this interface for reducing Runtime workload
		BufferedReader br;
		IModify mof;
		String algorithm = "";
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(SensaRun.subjectDir+ "/sensa.cfg")));
			String cfg = br.readLine();
			while(cfg != null){

				if(cfg.indexOf("mod_alg") != -1){					
					cfg = br.readLine();
					algorithm = cfg;
					cfg = br.readLine();
				}

				else if(cfg.indexOf("increment") != -1){
					cfg = br.readLine();
					if(type.equals("float"))
						increFloat = Float.parseFloat(cfg);
					else if(type.equals("double"))
						increDouble = Double.parseDouble(cfg);
					else if(type.equals("short"))
						increShort = Short.parseShort(cfg);
					else if(type.equals("long"))
						increLong = Long.parseLong(cfg);
					else if(type != null)
						increInt = Integer.parseInt(cfg);		
					cfg = br.readLine();

				}
				else if(cfg.indexOf("min") != -1){
					cfg = br.readLine();
					if(type.equals("float"))
						minLimitFloat = Float.parseFloat(cfg);
					else if(type.equals("double"))
						minLimitDouble = Double.parseDouble(cfg);
					else if(type.equals("short"))
						minLimitShort = Short.parseShort(cfg);
					else if(type.equals("long"))
						minLimitLong = Long.parseLong(cfg);
					else if(type.equals("byte"))
						minLimitByte = Byte.parseByte(cfg);
					else if(type != null)
						minLimitInt = Integer.parseInt(cfg);
					
					cfg = br.readLine();
				}
				else if(cfg.indexOf("max") != -1){
					cfg = br.readLine();
					if(type.equals("float"))
						maxLimitFloat = Float.parseFloat(cfg);
					else if(type.equals("double"))
						maxLimitDouble = Double.parseDouble(cfg);
					else if(type.equals("short"))
						maxLimitShort = Short.parseShort(cfg);
					else if(type.equals("long"))
						maxLimitLong = Long.parseLong(cfg);
					else if(type.equals("byte"))
						maxLimitByte = Byte.parseByte(cfg);
					else if(type != null)
						maxLimitInt = Integer.parseInt(cfg);	
					
					cfg = br.readLine();
				}
				else cfg = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean bForcible = true;
		final int MAXTRIAL = 20, TIMEROUT=10000;
		
		if(algorithm.equals("rand"))
			mof = new ModifyRandom();
		else if(algorithm.equals("inc")) 
			mof = new ModifyIncrement();
		else {
			mof = new ModifyObserved();
			bForcible = false;
		}
		*/
		// For random strategies, from the 1st occurrence (of the modify invocation), all occurrences 
		// afterwards will use the same random value picked up in the 1st one, which makes
		// the change behavior of Sensa closer to realistic occasions 
		if (bKeepPreValue && SensaRun.callCounter >= 2) {
			return;
		}
		
		// -- changed by hcai
		if(type.equals("int")){ 		
			write_Obeserved(preInt);
			int i = 0;
			returnInt = mof.intModify(preInt,runM, increInt, minLimitInt, maxLimitInt);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnInt);
			//while(!iffind && i < 20){
			while(!iffind && i < TIMEROUT){
				i++;
				if ( !bForcible &&  i >= MAXTRIAL ) {
					SensaRun.ifStop = true;
					break;
				}
				//returnInt = mof.intModify(preInt,runM, increInt, minLimitInt, maxLimitInt);
				/* note that increInt is actually used only in ModifyInrement, we adopt this change to increase the
				 * possibility of finding a distinct value quickly; This rationale applied to the similar following changes 
				 */
				//returnInt = mof.intModify(preInt, runM, r.nextInt(increInt*MAXTRIAL)*(r.nextInt()%2==0?1:-1), minLimitInt, maxLimitInt);
				returnInt = mof.intModify(preInt, runM+i, increInt*i, minLimitInt, maxLimitInt);
				iffind = SensaRun.valuestried(testN,returnInt);
			}
			/*
			if(i == 20)
				SensaRun.ifStop = true;
			*/
			SensaRun.ifStop = (TIMEROUT == i);
		}
		

		else if(type.equals("float")){
			write_Obeserved(preFloat);
			int i = 0;
			returnFloat = mof.floatModify(preFloat,runM, increFloat,minLimitFloat,maxLimitFloat);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnFloat);
			//while(!iffind && i < 20){
			while(!iffind && i < TIMEROUT){
				i++;
				if ( !bForcible &&  i >= MAXTRIAL ) {
					SensaRun.ifStop = true;
					break;
				}
				//returnFloat = mof.floatModify(preFloat,runM, increFloat,minLimitFloat,maxLimitFloat);
				returnFloat = mof.floatModify(preFloat,runM+i, increFloat*i,minLimitFloat,maxLimitFloat);
				iffind = SensaRun.valuestried(testN,returnFloat);
			}
			/*
			if(i == 20){
				SensaRun.ifStop = true;
			}
			*/
			SensaRun.ifStop = (TIMEROUT == i);
		}
		
		
		else if(type.equals("double")){
			write_Obeserved(preDouble);
			int i = 0;
			returnDouble = mof.doubleModify(preDouble,runM, increDouble,minLimitDouble,maxLimitDouble);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnDouble);
			//while(!iffind && i < 20){
			while(!iffind && i < TIMEROUT){
				i++;
				if ( !bForcible &&  i >= MAXTRIAL ) {
					SensaRun.ifStop = true;
					break;
				}
				//returnDouble = mof.doubleModify(preDouble,runM, increDouble,minLimitDouble,maxLimitDouble);
				returnDouble = mof.doubleModify(preDouble,runM+i, increDouble*i,minLimitDouble,maxLimitDouble);
				iffind = SensaRun.valuestried(testN,returnDouble);
			}
			/*
			if(i == 20)
				SensaRun.ifStop = true;
			*/
			SensaRun.ifStop = (TIMEROUT == i);
		}

				
		else if(type.equals("short")){
			write_Obeserved(preShort); // added by hcai
			int i = 0;
			returnShort = mof.shortModify(preShort,runM, increShort,minLimitShort,maxLimitShort);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnShort);
			//while(!iffind && i < 20){
			while(!iffind && i < TIMEROUT){
				i++;
				if ( !bForcible &&  i >= MAXTRIAL ) {
					SensaRun.ifStop = true;
					break;
				}
				//returnShort = mof.shortModify(preShort,runM, increShort,minLimitShort,maxLimitShort);
				returnShort = mof.shortModify(preShort,runM+i, (short) (increShort*i),minLimitShort,maxLimitShort);
				iffind = SensaRun.valuestried(testN,returnShort);
			}
			/*
			if(i == 20)
				SensaRun.ifStop = true;
			*/
			SensaRun.ifStop = (TIMEROUT == i);
		}
		
		
		else if(type.equals("long")){
			write_Obeserved(preLong);
			int i = 0;
			returnLong = mof.longModify(preLong,runM, increLong,minLimitLong,maxLimitLong);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnLong);
			//while(!iffind && i < 20){
			while(!iffind && i < TIMEROUT){
				i++;
				if ( !bForcible &&  i >= MAXTRIAL ) {
					SensaRun.ifStop = true;
					break;
				}
				//returnLong = mof.longModify(preLong,runM, increLong,minLimitLong,maxLimitLong);
				returnLong = mof.longModify(preLong,runM+i, increLong*i,minLimitLong,maxLimitLong);
				iffind = SensaRun.valuestried(testN,returnLong);
			}
			/*
			if(i == 20)
				SensaRun.ifStop = true;
			*/
			SensaRun.ifStop = (TIMEROUT == i);
		}
		// ----- end of hcai's change section
		
		else if(type.equals("char")){
			write_Obeserved(preChar);
			int i = 0;
			returnChar = mof.charModify(preChar,increInt);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnChar);
			while(!iffind && i < 20){
				i++;
				returnChar = mof.charModify(preChar,increInt);
				iffind = SensaRun.valuestried(testN,returnChar);
			}
			if(i == 20)
				SensaRun.ifStop = true;
		}

		
		else if(type.equals("byte")){
			write_Obeserved(preByte);
			int i = 0;
			returnByte = mof.byteModify(preByte,runM, increByte, minLimitByte, maxLimitByte);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnByte);
			while(!iffind && i < 20){
				i++;
				returnByte = mof.byteModify(preByte,runM, increByte, minLimitByte, maxLimitByte);
				iffind = SensaRun.valuestried(testN,returnByte);
			}
			if(i == 20)
				SensaRun.ifStop = true;
		}
		
		else if(type.equals("boolean")){
			write_Obeserved(preBool);
			returnBool = mof.boolModify(preBool);
			// -- hcai
			//SensaRun.valuestried(testN,returnBool);
			if (! SensaRun.valuestried(testN,returnBool) ) {
				SensaRun.ifStop = true;
			}
			// ---
		}
		
		else if(preObject instanceof String){
			preString = (String)preObject;
			write_Obeserved(preString);
			
			int i = 0;
			returnString = mof.stringModify(preString,runM);
			boolean iffind = false;
			iffind = SensaRun.valuestried(testN,returnString);
			while(!iffind && i < 20){
				i++;
				returnString = mof.stringModify(preString,runM);
				iffind = SensaRun.valuestried(testN,returnString);
			}
			if(i == 20)
				SensaRun.ifStop = true;
			returnObject = returnString;
		}
		
		/**
		 *  TODO: You could create your additional types for Sensa to support.
		 */
		else{
			if(preObject == null){
				write_Obeserved(preObject);
			}
			returnObject = mof.objectModify(preObject);
			// -- hcai
			if (! SensaRun.valuestried(testN,returnObject) ) {
				SensaRun.ifStop = true;
			}
			// ---
			System.out.println(returnObject == null);
		}
	}
	
	private static void write_Obeserved(Object o){
		String fileName = SensaRun.subjectDir + "/observed_value.out";

		try {
			RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
			String value = randomFile.readLine();
			
			while(value != null && !value.equals(null)){
				if(value.equals("!!!NULL") && o == null) {
					// --- added by hcai
					randomFile.close();
					// ---
					return;
				}
				if(value.equals(o.toString())){
					// --- added by hcai
					randomFile.close();
					// ---
					return;
				}
				value = randomFile.readLine();
			}
			
			String content;
			
			if(o == null){
				System.out.println("I am null!");
				content = "!!!NULL\r\n";
			}
			else 
				content = o.toString() + "\r\n";

			long fileLength = randomFile.length();
		    randomFile.seek(fileLength);
		    
		    randomFile.writeBytes(content);
		    randomFile.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
