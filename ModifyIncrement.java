package Sensa;
/**
 * x --> x+increment, x-increment, x+2*increment, x-2*increment....
 * @author swhite
 * by default, it applies the same change in every iteration in the same run
 */
public class ModifyIncrement implements IModify {

	@Override
	public int intModify(int pre, int runM, int x, int minLimit, int maxLimit) {
		if(runM % 2 == 0){
			pre = pre + x * (runM/2);
		}
		else pre = pre - x * (runM/2);
		if(pre<minLimit || pre>maxLimit) SensaRun.ifStop = true;
		return pre;
	}

	@Override
	public float floatModify(float pre, int runM, float x, float minLimit, float maxLimit) {
		if(runM % 2 == 0){
			pre = pre + x * (runM/2);
		}
		else pre = pre - x * (runM/2);
		if(pre<minLimit || pre>maxLimit) SensaRun.ifStop = true;
		return pre;
	}


	@Override
	public long longModify(long pre, int runM, long x, long minLimit, long maxLimit) {
		if(runM % 2 == 0){
			pre = pre + x * (runM/2);
		}
		else pre = pre - x * (runM/2);
		if(pre<minLimit || pre>maxLimit) SensaRun.ifStop = true;
		return pre;
	}

	@Override
	public short shortModify(short pre, int runM, short x, short minLimit, short maxLimit) {
		if(runM % 2 == 0){
			pre = (short)(pre + x * (runM/2));
		}
		else pre = (short)(pre - x * (runM/2));
		if(pre<minLimit || pre>maxLimit) SensaRun.ifStop = true;
		return pre;
	}

	@Override
	public char charModify(char pre, int runM) {
		pre = (char)(Math.abs(((int)pre + runM)%95) + 10);
		return pre;
	}

	@Override
	public double doubleModify(double pre, int runM, double x, double minLimit,
			double maxLimit) {
		if(runM % 2 == 0){
			pre = pre + x * (runM/2);
		}
		else pre = pre - x * (runM/2);
		if(pre<minLimit || pre>maxLimit) SensaRun.ifStop = true;
		return pre;
	}

	@Override
	public byte byteModify(byte pre, int runM, byte x, byte minLimit, byte maxLimit) {
		if(runM % 2 == 0){
			pre = (byte)(pre + x * (runM/2));
		}
		else pre = (byte)(pre - x * (runM/2));
		if(pre<minLimit || pre>maxLimit) SensaRun.ifStop = true;
		return pre;
	}

	@Override
	public String stringModify(String pre, int runM) {
		String returnS = pre;
		for(int i=1;i<runM;i++)
			returnS += pre;
		System.out.println(returnS + "!!!!");
		return returnS;
	}

	@Override
	public boolean boolModify(boolean pre) {
		// TODO Auto-generated method stub
		if(pre) return false;
		else return true;
	}

	@Override
	public Object objectModify(Object pre) {
		return null;
	}

}
