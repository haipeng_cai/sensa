import java.io.*;

import profile.BranchReporter;
import profile.DUAReporter;
import profile.ExecHistReporter;
public class Dummy {
	static void __link() { Sensa.Modify.__link(); ExecHistReporter.__link();
			BranchReporter.__link(); DUAReporter.__link();}
  	public static void main(String[] args) {
  		System.out.println("This a test java class feeding the Sensa tool.");
  		int x = 4;
  		int y = 12;
  		if (x > y) {
  			System.out.println("X is greater than Y.");
  		}
  		else {
  			System.out.println("X is less than or equal to Y.");
  		    System.out.println("All done.");
  		}    
    }
}