package Sensa;
/**
 * 
 * @author swhite
 *
 * User could inherit from this default modify to create his/her own modification algorithm
 * It returns the same value given.
 */
public class ModifyDefault implements IModify {

	@Override
	public int intModify(int pre, int runM, int x, int minLimit, int maxLimit) {
		return pre;
	}

	@Override
	public float floatModify(float pre, int runM, float x, float minLimit, float maxLimit) {
		return pre;
	}

	@Override
	public long longModify(long pre, int runM, long x, long minLimit, long maxLimit) {
		return pre;
	}

	@Override
	public short shortModify(short pre, int runM, short x, short minLimit, short maxLimit) {
		return pre;
	}

	@Override
	public char charModify(char pre, int runM) {
		return pre;
	}

	@Override
	public double doubleModify(double pre, int runM, double x, double minLimit,
			double maxLimit) {
		return pre;
	}

	@Override
	public byte byteModify(byte pre, int runM, byte x, byte minLimit, byte maxLimit) {
		return pre;
	}

	@Override
	public String stringModify(String pre, int runM) {
		return pre;
	}
	
	@Override
	public boolean boolModify(boolean pre) {
		return pre;
	}

	@Override
	public Object objectModify(Object pre) {
		return pre;
	}

}
