package Sensa;
/**
 * By Swhite_zyj July 2012
 * 
 * Revisions:
 *	@August 26th by Hcai
 *		- fix the command line parameter handling issues for "left_right_for_ifStmt"
 *	@August 29th by Hcai
 *		- use GlobalStmtId to match the given "change location" as the insertion point for instrumentation
 *	@Sept. 3rd by Hcai
 *		- discard the "change variable name", "change location" and "statement type" parameters and, only use the
 *			"start" parameter to locate the insertion point for the instrumentation 
 *		- for statement type not currently supported, give users pertinent admonitory info
 * @Sept. 5th by Hcai
 * 		- change to support instrument the modifications at a non-Local soot-Value like Constant in the IfStmt.
 * SensaInst calls Soot.Main to analyze
 */

//import java.util.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import profile.ExecHistInstrumenter;

import dua.Extension;
import dua.Forensics;
import dua.global.ProgramFlowGraph;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

import soot.*;
//import soot.JastAddJ.GEExpr;
import soot.jimple.*;
import soot.util.*;

public class SensaInst implements Extension {
	
	static SootClass modifyCla;
	static SootMethod modifyMet;
	static SootField ch_value;
	static SootField ar_value;
	static String changeValue = "";
	static int changeLocation;
	static String changeStmtType = "";

	static boolean leftToModify = false; // this will be used only if the target statement is of "ifStmt", for now
	
	public static void main(String args[]){
		args = preProcessArgs(args);

		SensaInst sensaInst = new SensaInst();
		Forensics.registerExtension(sensaInst);
		Forensics.main(args);
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = SensaOptions.process(args);
		
		/* --- these should be identified or retrieved automatically
		changeValue = args[0];
		changeLocation = Integer.parseInt(args[1]);
		changeStmtType = args[2];
		*/
		//System.out.println("changevalue = " + changeValue + "  changeLocation = " + changeLocation);
		
		// add option to consider formal parameters as defs and return values as uses, for reaching def/use analysis
		//     and option to not remove repeated branches in switch nodes
		String[] args2;
		/* commented out by hcai
		if(!changeStmtType.equals("ifStmt")){
			args2 = new String[args.length-1];
			System.arraycopy(args, 3, args2, 0, args.length-3);
			args2[args.length - 3] = "-paramdefuses";
			args2[args.length - 2] = "-keeprepbrs";
		}
		else{
			if(args[3].equals("left"))
				leftToModify = true;
			args2 = new String[args.length-2];
			System.arraycopy(args, 4, args2, 0, args.length-4);
			args2[args.length - 4] = "-paramdefuses";
			args2[args.length - 3] = "-keeprepbrs";
		}
		*/
		/*
		// --- added by hcai 
		int offset = 0;
		if ( changeStmtType.equals("ifStmt") && args[3].equals("left") ) {
			leftToModify = true;
			offset = 1;
		}
		args2 = new String[args.length-1-offset];
		System.arraycopy(args, 3+offset, args2, 0, args.length-3-offset);
		args2[args.length - 3 - offset] = "-paramdefuses";
		args2[args.length - 2 - offset] = "-keeprepbrs";
		// --- 
		*/
		// --- added by hcai 
		int offset = 0;
		if ( args[0].equalsIgnoreCase("left") || args[0].equalsIgnoreCase("right") ) {
			offset = 1;
			leftToModify = args[0].equalsIgnoreCase("left");
		}
		args2 = new String[args.length + 1 - offset]; /* here 1 is for the -exechist option */
		System.arraycopy(args, offset, args2, 0, args.length-1-offset);
		args2[args.length - offset] = "-paramdefuses";
		args2[args.length - 1 - offset] = "-keeprepbrs";
		// --- 
		
		args = args2;
		return args;
	}
	
	@Override public void run() {
		System.out.println("Running sensability analysis extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		
		// for now, assume exactly one start point is provided
		DependenceGraph depGraph = null;
		if (SensaOptions.doExecHist()) {
			CFGNode nStart = StmtMapper.getNodeFromGlobalId( SensaOptions.getStartStmtIds().get(0) );
			depGraph = new DependenceGraph(new NodePoint(nStart, NodePoint.POST_RHS), -1);
			System.out.println("Start:  "+nStart); 
		}
		
		modifyCla = Scene.v().getSootClass("Sensa.Modify");
		modifyMet = modifyCla.getMethodByName("modify");

		/*
		Stmt stmt2 = StmtMapper.getStmtFromGlobalId( SensaOptions.getStartStmtIds().get(0) );
		SootMethod method = ProgramFlowGraph.inst().getContainingMethod(stmt2);
		*/
		Stmt stmt = StmtMapper.getStmtFromGlobalId( SensaOptions.getStartStmtIds().get(0) );
		SootMethod method = ProgramFlowGraph.inst().getContainingMethod(stmt);
		
		/* 
		 * check if the target statement is of the type currently supported or not BEFORE doing too much and, by the way,
		 * fetch the variable targeted
		 */
		Local tmpvalue;
		//Value tmpvalue;
		if ( stmt instanceof IfStmt ) {
			// combinatorial conditional expression is not supported for now
			ConditionExpr condExp = (ConditionExpr)((IfStmt) stmt).getCondition();
			
			// if the user-specified side is a constant, will target the other side whatsoever, since we do not instrument for a constant
			// we still follow the "right side" default when this option is not specified
			if ( leftToModify  && ( ! (condExp.getOp1() instanceof Local ) ) ) leftToModify = false;
			if ( !leftToModify  && ( ! (condExp.getOp2() instanceof Local ) ) ) leftToModify = true;
			
			if ( (leftToModify  && ( ! (condExp.getOp1() instanceof Local ) )) || 
				 (!leftToModify &&	! (condExp.getOp2() instanceof Local ) ) ) {
					System.err.println("Illegal instrumentation: neither side of the conditional expression in the given statement is a local.");
					return;
					//System.out.println("Warning: the target soot-Value is NOT a local.");
			}
						
			tmpvalue = (Local) (leftToModify? condExp.getOp1() : condExp.getOp2());
			//tmpvalue = (leftToModify? condExp.getOp1() : condExp.getOp2());
		}
		else if ( stmt instanceof AssignStmt ) {
			// for an "assignment" statement, we always target the defined variable (on the left side)
			tmpvalue = (Local)( ((AssignStmt)stmt).getLeftOp() );
			//tmpvalue = ( ((AssignStmt)stmt).getLeftOp() );
		}
		else {
			System.err.println("Statement at the given point, line " + 
					SensaOptions.getStartStmtIds().get(0) + ", is not currently upported : " + stmt.toString());
			return;
		}
			
		// for debug only
		String varName;
		if (tmpvalue instanceof Local) {
			varName = ((Local) tmpvalue).getName();
		} else {
			varName = tmpvalue.toString();
		}
		System.out.println("The target statement is: " + stmt.toString() + "; and the target soot-Value is: " + varName);
		
		Body body = method.getActiveBody();
		/*
		Chain units = body.getUnits();
		*/
		Chain<Unit> units = (Chain<Unit>)(body.getUnits());
		//Iterator stmtIt = units.snapshotIterator();
		
		/*
		Chain local = body.getLocals();
		*/
		Chain<Local> local = body.getLocals();
		//Iterator it = local.snapshotIterator();
		
		/*
		Local tmpvalue = (Local)it.next();
		while(it.hasNext()){
			if(tmpvalue.getName().equals(changeValue)) break;	
			tmpvalue = (Local)it.next();
			
		}
		*/
		
		String type = tmpvalue.getType().toString();
		System.out.println("Type of the target variable ............ "+type);
		boolean isObject=false;
		
		if(type.equals("int")){
			ch_value = modifyCla.getFieldByName("returnInt");
			ar_value = modifyCla.getFieldByName("preInt");
		}
		else if(type.equals("float")){
			ch_value = modifyCla.getFieldByName("returnFloat");
			ar_value = modifyCla.getFieldByName("preFloat");
		}
		else if(type.equals("short")){
			ch_value = modifyCla.getFieldByName("returnShort");
			ar_value = modifyCla.getFieldByName("preShort");
		}
		else if(type.equals("double")){
			ch_value = modifyCla.getFieldByName("returnDouble");
			ar_value = modifyCla.getFieldByName("preDouble");
		}
		else if(type.equals("char")){
			ch_value = modifyCla.getFieldByName("returnChar");
			ar_value = modifyCla.getFieldByName("preChar");
		}
		else if(type.equals("byte")){
			ch_value = modifyCla.getFieldByName("returnByte");
			ar_value = modifyCla.getFieldByName("preByte");
		}	
		else if(type.equals("boolean")){
			ch_value = modifyCla.getFieldByName("returnBool");
			ar_value = modifyCla.getFieldByName("preBool");
		}	
		// Object type could match other types, such as String, List, Map and other types user defines.
		else {
			ch_value = modifyCla.getFieldByName("returnObject");
			ar_value = modifyCla.getFieldByName("preObject");
			isObject=true;
		}

		//----- added by hcai
		/*
		int nInsertionLoc = changeLocation;
		Stmt stmt = StmtMapper.getStmtFromGlobalId( nInsertionLoc );
		*/

		//if((stmt instanceof IfStmt) && (changeStmtType.equals("ifStmt"))) {
		if( stmt instanceof IfStmt ) {	
			IfStmt ifStmtBeforeMod = (IfStmt)stmt;
			Expr expr = (Expr)ifStmtBeforeMod.getCondition();

			Value tmp = (Value)(Jimple.v().newStaticFieldRef(ch_value.makeRef()));
			Local tmpV=Jimple.v().newLocal("tmpv", tmp.getType());
			local.add(tmpV);

			/*
			ConditionExpr eq = (ConditionExpr)expr;
			if(leftToModify) tmp = eq.getOp1();
			else tmp = eq.getOp2();
			*/
			
			//AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(ar_value.makeRef()),tmp);
			AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(ar_value.makeRef()), tmpvalue);
			units.insertBefore(insert1, stmt);	
			
			InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
			Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
			
			units.insertBefore(invStmt, stmt);
			
			Stmt store = Jimple.v().newAssignStmt(tmpV, Jimple.v().newStaticFieldRef(ch_value.makeRef()));
			units.insertBefore(store, stmt);
			
			if(leftToModify) ((ConditionExpr) expr).setOp1(tmpV);
			else ((ConditionExpr) expr).setOp2(tmpV);
			
			ifStmtBeforeMod.setCondition(expr);
		}
		else {
			//get the previous value from test subject, e.g ScheduleClass
			AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(ar_value.makeRef()),tmpvalue);
			/*
			units.insertBefore(insert1, stmt);
			*/
			
			InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
			Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
			/*
			units.insertBefore(invStmt, stmt);//Uses Before because we have to insert many statements!
			*/
		
			//Get the return value of the user method modify!
			if (isObject) {
				Local tmpobject = Jimple.v().newLocal("tmpobject",RefType.v("java.lang.Object"));
				local.add(tmpobject);
				
				AssignStmt getReturnValue_ = Jimple.v().newAssignStmt(tmpobject,Jimple.v().newStaticFieldRef(ch_value.makeRef()));
				/*
				units.insertBefore(getReturnValue_, stmt);
				*/
				AssignStmt getReturnValue = Jimple.v().newAssignStmt(tmpvalue,Jimple.v().newCastExpr(tmpobject, tmpvalue.getType()));
				units.insertAfter(getReturnValue, stmt);
				
				units.insertAfter(getReturnValue_, stmt);
				units.insertAfter(invStmt, stmt);
				units.insertAfter(insert1, stmt);
			} 
			else	{
				AssignStmt getReturnValue = Jimple.v().newAssignStmt(tmpvalue,Jimple.v().newStaticFieldRef(ch_value.makeRef()));
				/*
				units.insertBefore(getReturnValue, stmt);
				*/
				units.insertAfter(getReturnValue, stmt);
				units.insertAfter(invStmt, stmt);
				units.insertAfter(insert1, stmt);
			}
		}
		// --

		/* commented out by hcai
		int i = 0;// temporary to find where to insert		

		while(stmtIt.hasNext()){
			Stmt stmt = (Stmt)stmtIt.next();
			i++;
			System.out.println("****" + i+": "+stmt.toString());
			//if(i == changeLocation) break;
			if(i == changeLocation){
				if((stmt instanceof IfStmt) && (changeStmtType.equals("ifStmt"))){

					IfStmt ifStmtBeforeMod = (IfStmt)stmt;
					Expr expr = (Expr)ifStmtBeforeMod.getCondition();

					Value tmp = (Value)(Jimple.v().newStaticFieldRef(ch_value.makeRef()));
					Local tmpV=Jimple.v().newLocal("tmpv", tmp.getType());
					local.add(tmpV);
					
					if(expr instanceof EqExpr){
						EqExpr eq = (EqExpr)expr;
						if(leftToModify) tmp = eq.getOp1();
						else tmp = eq.getOp2();
						AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef
								(ar_value.makeRef()),tmp);
						units.insertBefore(insert1, stmt);			
						
						InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
						Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
						
						units.insertBefore(invStmt, stmt);
						
						Stmt store = Jimple.v().newAssignStmt(tmpV, Jimple.v().newStaticFieldRef(ch_value.makeRef()));
						units.insertBefore(store, stmt);
						
						if(leftToModify) ((EqExpr) expr).setOp1(tmpV);	
						else ((EqExpr) expr).setOp2(tmpV);	
						
						ifStmtBeforeMod.setCondition(expr);
					}
					else if(expr instanceof GtExpr){
						GtExpr eq = (GtExpr)expr;
						if(leftToModify) tmp = eq.getOp1();
						else tmp = eq.getOp2();
						AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef
								(ar_value.makeRef()),tmp);
						units.insertBefore(insert1, stmt);			
						
						InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
						Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
						
						units.insertBefore(invStmt, stmt);
						
						Stmt store = Jimple.v().newAssignStmt(tmpV, Jimple.v().newStaticFieldRef(ch_value.makeRef()));
						units.insertBefore(store, stmt);
						
						if(leftToModify) ((EqExpr) expr).setOp1(tmpV);	
						else ((GtExpr) expr).setOp2(tmpV);	
						
						ifStmtBeforeMod.setCondition(expr);	
					}
					else if(expr instanceof LtExpr){
						LtExpr eq = (LtExpr)expr;
						if(leftToModify) tmp = eq.getOp1();
						else tmp = eq.getOp2();
						AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef
								(ar_value.makeRef()),tmp);
						units.insertBefore(insert1, stmt);			
						
						InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
						Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
						
						units.insertBefore(invStmt, stmt);
						
						Stmt store = Jimple.v().newAssignStmt(tmpV, Jimple.v().newStaticFieldRef(ch_value.makeRef()));
						units.insertBefore(store, stmt);
						
						if(leftToModify) ((LtExpr) expr).setOp1(tmpV);	
						else ((LtExpr) expr).setOp2(tmpV);	
						ifStmtBeforeMod.setCondition(expr);	
						
					}
					else if(expr instanceof GeExpr){
						GeExpr eq = (GeExpr)expr;
						if(leftToModify) tmp = eq.getOp1();
						else tmp = eq.getOp2();
						AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef
								(ar_value.makeRef()),tmp);
						units.insertBefore(insert1, stmt);	
						
						InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
						Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
						
						units.insertBefore(invStmt, stmt);

						Stmt store = Jimple.v().newAssignStmt(tmpV, Jimple.v().newStaticFieldRef(ch_value.makeRef()));
						units.insertBefore(store, stmt);

						if(leftToModify) ((GeExpr) expr).setOp1(tmpV);	
						else ((GeExpr) expr).setOp2(tmpV);		
						ifStmtBeforeMod.setCondition(expr);	
						
					}
					else if(expr instanceof LeExpr){
						LeExpr eq = (LeExpr)expr;
						if(leftToModify) tmp = eq.getOp1();
						else tmp = eq.getOp2();
						AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef
								(ar_value.makeRef()),tmp);
						units.insertBefore(insert1, stmt);			
						
						InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
						Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
						
						units.insertBefore(invStmt, stmt);
						
						Stmt store = Jimple.v().newAssignStmt(tmpV, Jimple.v().newStaticFieldRef(ch_value.makeRef()));
						units.insertBefore(store, stmt);
						
						if(leftToModify) ((LeExpr) expr).setOp1(tmpV);	
						else ((LeExpr) expr).setOp2(tmpV);	
						ifStmtBeforeMod.setCondition(expr);	
					}
					break;
				}
				//get the previous value from test subject, e.g ScheduleClass
				int iii=1;
				AssignStmt insert1 = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef
						(ar_value.makeRef()),tmpvalue);
				units.insertBefore(insert1, stmt);			
				
				InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(modifyMet.makeRef(),StringConstant.v(type)); 
				Stmt invStmt = Jimple.v().newInvokeStmt(invokeExpr);
				
				units.insertBefore(invStmt, stmt);//Uses Before because we have to insert many statements!
			
				//Get the return value of the user method modify!
				if (isObject)
				{
					AssignStmt getReturnValue_ = Jimple.v().newAssignStmt(tmpobject,Jimple.v().newStaticFieldRef
							(ch_value.makeRef()));				
					units.insertBefore(getReturnValue_, stmt);
					AssignStmt getReturnValue = Jimple.v().newAssignStmt(tmpvalue,Jimple.v().newCastExpr(tmpobject, tmpvalue.getType()));
					units.insertAfter(getReturnValue, stmt);
				}
				else
				{
					AssignStmt getReturnValue = Jimple.v().newAssignStmt(tmpvalue,Jimple.v().newStaticFieldRef
						(ch_value.makeRef()));				
					units.insertBefore(getReturnValue, stmt);
				}
			}

		}
		*/

		if (SensaOptions.doExecHist()) {
			// instrument subject to output augmented execution histories of sliced statements for dynamic change-impact assessment
			ExecHistInstrumenter instr = new ExecHistInstrumenter();
			instr.instrument(depGraph.getPointsInSlice());
						
			// -- hcai
			// just print all slices by stmt ids
			List<NodePoint> pntsSorted = new ArrayList<NodePoint>(depGraph.getPointsInSlice());
			Collections.sort(pntsSorted, NodePoint.NodePointComparator.inst);
			File fOut = new File(dua.util.Util.getCreateBaseOutPath() + "fwdslice.out");
			try {
				Writer writer = new FileWriter(fOut);
				
				Collections.sort(pntsSorted, NodePointComparator.inst);
				for (NodePoint pTgt : pntsSorted) {
					String stmtid = pTgt.toString();
					if (pTgt.getRhsPos() == NodePoint.PRE_RHS) writer.write("-");
					writer.write(stmtid.substring(0, stmtid.indexOf('[')) + "\n");
				}
				
				writer.flush();
				writer.close();
			} catch (IOException e) { e.printStackTrace(); }
			// --
		}
	}
}
