package Sensa;
/** It is an interface for all Modify Algorithm to inherit
 *  It contains eight methods to support eight different types.
 *  If you want to extend Sensa to other types, this is the first step. 
*/

public interface IModify {

	public int intModify(int pre, int runM, int x, int minLimit, int maxLimit);
	
	public float floatModify(float pre, int runM, float x, float minLimit, float maxLimit);
	
	public long longModify(long pre, int runM, long x, long minLimit, long maxLimit);
	
	public short shortModify(short pre, int runM, short x, short minLimit, short maxLimit);
	
	public char charModify(char pre, int runM);
	
	public double doubleModify(double pre, int runM, double x, double minLimit, double maxLimit);
	
	public byte byteModify(byte pre, int runM, byte x, byte minLimit, byte maxLimit);
	
	public boolean boolModify(boolean pre);
	
	public String stringModify(String pre, int runM);
	
	public Object objectModify(Object pre);
}
