package Sensa;
import java.util.Random;

/*
 * By swhite_zyj July 2012
 * 
 * Revisions:
 * @Sept. 6th by Hcai
 * 		- changes in  {int, float, double, long, short}Modify() made to avoid "value out of range limited the data type 
 * 		(for example, when using the default, in intModify(), maxLimit-minLimit will cause such an issue) that lead to 
 * 		the failure in randomizing the values (always return 0! ). Similar reasons for all other changes in this sort.
 * @Dec. 9th by hcai
 *		- adapt to be compiled with Jdk1.4.2
 */ 

public class ModifyRandom implements IModify {
	
	static Random r = new Random();
	
	public ModifyRandom() {
		r.setSeed(System.currentTimeMillis());
	}

	
	public int intModify(int pre, int runM, int x, int minLimit, int maxLimit) {
		//int tmp = r.nextInt() % (Math.max(maxLimit-minLimit, maxLimit+minLimit));
		int tmp = r.nextInt() % (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
		boolean flag = (tmp >= minLimit && tmp <= maxLimit);
		while(!flag){
			//tmp = r.nextInt()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
			tmp = r.nextInt() % (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
			flag = (tmp >= minLimit && tmp <= maxLimit);
		}
		return tmp;
	}

	
	public float floatModify(float pre, int runM, float x, float minLimit, float maxLimit) {
		//float tmp = r.nextFloat()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
		float tmp = r.nextFloat()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
		boolean flag = (tmp >= minLimit && tmp <= maxLimit);
		while(!flag){
			//tmp = r.nextFloat()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
			tmp = r.nextFloat()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
			flag = (tmp >= minLimit && tmp <= maxLimit);
		}
		return tmp + r.nextInt();
	}

	
	public long longModify(long pre, int runM, long x, long minLimit, long maxLimit) {
		//long tmp = r.nextLong()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
		long tmp = r.nextLong()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
		boolean flag = (tmp >= minLimit && tmp <= maxLimit);
		while(!flag){
			//tmp = r.nextLong()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
			tmp = r.nextLong()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
			flag = (tmp >= minLimit && tmp <= maxLimit);
		}
		return tmp;
	}

	
	public short shortModify(short pre, int runM, short x, short minLimit, short maxLimit) {
		//short tmp = (short)(r.nextInt()% (Math.max(maxLimit-minLimit, maxLimit+minLimit)));
		short tmp = (short)(r.nextInt()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit)));
		boolean flag = (tmp >= minLimit && tmp <= maxLimit);
		while(!flag){
			//tmp = (short)(r.nextInt()% (Math.max(maxLimit-minLimit, maxLimit+minLimit)));
			tmp = (short)(r.nextInt()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit)));
			flag = (tmp >= minLimit && tmp <= maxLimit);
		}
		return tmp;
	}

	private int cseed = 0;
	
	public char charModify(char pre, int runM) {
		//int i = Math.abs(r.nextInt()%95) + 10; //commented by Siyuan
		//pre = (char)i; //commented by Siyuan
		// -- Siyuan
		
		Random rc = new Random(System.nanoTime() + cseed);
		cseed ++;
		int ascii_code = rc.nextInt(126);
		pre = (char)ascii_code;
		//System.out.println( (int)(pre) + "!!!!!!");
		//--
		
		return pre;
	}

	
	public double doubleModify(double pre, int runM, double x, double minLimit,
			double maxLimit) {
		//double tmp = r.nextDouble()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
		double tmp = r.nextDouble()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
		boolean flag = (tmp >= minLimit && tmp <= maxLimit);
		while(!flag){
			//tmp = r.nextDouble()% (Math.max(maxLimit-minLimit, maxLimit+minLimit));
			tmp = r.nextDouble()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit));
			flag = (tmp >= minLimit && tmp <= maxLimit);
		}
		return tmp + r.nextInt();
	}

	
	public byte byteModify(byte pre, int runM, byte x, byte minLimit, byte maxLimit) {
		//byte tmp = (byte)(r.nextInt()% (Math.max(maxLimit-minLimit, maxLimit+minLimit)));
		byte tmp = (byte)(r.nextInt()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit)));
		boolean flag = (tmp >= minLimit && tmp <= maxLimit);
		while(!flag){
			//tmp = (byte)(r.nextInt()% (Math.max(maxLimit-minLimit, maxLimit+minLimit)));
			tmp = (byte)(r.nextInt()% (Math.max(maxLimit/2-minLimit/2, maxLimit+minLimit)));
			flag = (tmp >= minLimit && tmp <= maxLimit);
		}
		return tmp;
	}
	
	
	public String stringModify(String pre, int runM) {
		// insert -- 0
		// append -- 1
		// delete -- 2
		//replace -- 3
		int tmp = r.nextInt() % 4;
		int ascii = r.nextInt()%95 + 32;  //valid ascii is from 32 to 126
		String chs = String.valueOf((char)ascii);
		
		if(tmp == 0){
			int IL = r.nextInt()%pre.length();
			String rs = pre.substring(0,IL);
			if(IL == pre.length()-1)
				rs += chs;
			else rs += chs + pre.substring(IL+1,pre.length()-1);	
			return rs;
		}
		else if(tmp == 1){
			pre += chs;
			return pre;
		}
		else if(tmp == 2){
			int IL = r.nextInt()%pre.length();
			String rs = pre.substring(0,IL-1);
			if(IL != pre.length()-1)
				rs += pre.substring(IL+1,pre.length()-1);	
			return rs;
		}
		else{
			int IL = r.nextInt()%pre.length();
			String rs = pre.substring(0,IL-1);
			if(IL != pre.length()-1)
				rs += chs + pre.substring(IL+1,pre.length()-1);	
			return rs;
		}
	}

	
	public boolean boolModify(boolean pre) {
		int i = r.nextInt();
		if(i%2 == 0)
			return true;
		else return false;
	}
	
	
	public Object objectModify(Object pre) {
		return null;
	}
	
}
